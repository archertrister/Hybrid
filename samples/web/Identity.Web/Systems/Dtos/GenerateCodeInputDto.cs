﻿// -----------------------------------------------------------------------
//  <copyright file="GenerateCodeInputDto.cs" company="ESoftor开源团队">
//      Copyright (c) 2014-2019 ESoftor. All rights reserved.
//  </copyright>
//  <site>http://www.lxking.cn</site>
//  <last-editor>郭明锋</last-editor>
//  <last-date>2019-01-06 22:37</last-date>
// -----------------------------------------------------------------------

namespace ESoftor.Web.Systems.Dtos
{
    /// <summary>
    /// 代码生成输入DTO
    /// </summary>
    public class GenerateCodeInputDto
    { }
}