﻿// -----------------------------------------------------------------------
//  <copyright file="SystemSettingKeys.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-06-25 21:26</last-date>
// -----------------------------------------------------------------------

namespace Hybrid.Core.Systems
{
    /// <summary>
    /// 系统设置项键值
    /// </summary>
    public class SystemSettingKeys
    {
        public const string SiteName = "Site.Name";
        public const string SiteDescription = "Site.Description";
    }
}