﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Web
{
    public interface IEnabled
    {
        bool Enabled { get; set; }
    }
}
