﻿// -----------------------------------------------------------------------
//  <copyright file="IAllAssemblyFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2017-08-17 0:45</last-date>
// -----------------------------------------------------------------------

namespace Hybrid.Reflection
{
    /// <summary>
    /// 定义所有程序集查找器
    /// </summary>
    public interface IAllAssemblyFinder : IAssemblyFinder
    { }
}