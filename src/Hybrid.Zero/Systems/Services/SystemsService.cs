﻿// -----------------------------------------------------------------------
//  <copyright file="SystemsService.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2019-01-08 13:37</last-date>
// -----------------------------------------------------------------------

namespace Hybrid.Zero.Systems.Services
{
    /// <summary>
    /// 业务实现：系统模块
    /// </summary>
    public class SystemsService : ISystemsService
    { }
}