﻿// -----------------------------------------------------------------------
//  <copyright file="RedisModule.cs" company="Hybrid开源团队">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-12-14 20:14</last-date>
// -----------------------------------------------------------------------

using System.ComponentModel;

namespace Hybrid.Redis
{
    /// <summary>
    /// Redis模块
    /// </summary>
    [Description("Redis模块")]
    public class RedisModule : RedisModuleBase
    { }
}