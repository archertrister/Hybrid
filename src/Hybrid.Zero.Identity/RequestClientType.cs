﻿namespace Hybrid.Zero.Identity
{
    /// <summary>
    /// 请求的客户端类型
    /// </summary>
    public enum RequestClientType
    {
        /// <summary>
        /// 浏览器类型
        /// </summary>
        Browser,

        /// <summary>
        /// 桌面客户端
        /// </summary>
        Desktop,

        /// <summary>
        /// 手机客户端
        /// </summary>
        Mobile
    }
}