﻿// -----------------------------------------------------------------------
//  <copyright file="ISystemsService.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2019-01-08 13:38</last-date>
// -----------------------------------------------------------------------

using Hybrid.Application.Services;

namespace Hybrid.Zero.Systems.Services
{
    /// <summary>
    /// 业务契约：系统模块
    /// </summary>
    public interface ISystemsService : IApplicationService
    {
    }
}