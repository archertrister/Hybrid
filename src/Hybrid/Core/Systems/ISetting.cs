// -----------------------------------------------------------------------
//  <copyright file="ISetting.cs" company="cn.lxking">
//      Copyright ? 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-08-24 11:56</last-date>
// -----------------------------------------------------------------------

namespace Hybrid.Core.Systems
{
    /// <summary>
    /// 定义设置信息
    /// </summary>
    public interface ISetting
    { }
}