﻿// -----------------------------------------------------------------------
//  <copyright file="SystemSetting.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-08-02 17:56</last-date>
// -----------------------------------------------------------------------

using Hybrid.Core.Systems;

namespace Hybrid.Zero.Systems
{
    /// <summary>
    /// 系统设置项
    /// </summary>
    public class SystemSetting : ISetting
    {
    }
}