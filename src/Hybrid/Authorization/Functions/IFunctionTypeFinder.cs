﻿// -----------------------------------------------------------------------
//  <copyright file="IFunctionTypeFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2017-09-14 12:43</last-date>
// -----------------------------------------------------------------------

using Hybrid.Reflection;

namespace Hybrid.Authorization.Functions
{
    /// <summary>
    /// 定义功能信息类型查找器
    /// </summary>
    public interface IFunctionTypeFinder : ITypeFinder
    { }
}