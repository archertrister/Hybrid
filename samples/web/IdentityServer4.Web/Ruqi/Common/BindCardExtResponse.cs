﻿namespace FlyingFish.Mobile.Ruqi.Common
{
    /// <summary>
    /// 跳转到机构绑卡响应模型
    /// </summary>
    public class BindCardExtResponse
    {
        /// <summary>
        /// 绑卡H5链接地址，点击该地址可直接打开绑卡页面
        /// </summary>
        public string url { get; set; }
    }
}