﻿// -----------------------------------------------------------------------
//  <copyright file="IEntityConfigurationTypeFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2017-08-17 2:20</last-date>
// -----------------------------------------------------------------------

using Hybrid.Reflection;

namespace Hybrid.EntityFrameworkCore
{
    /// <summary>
    /// 定义实体类配置类型查找器
    /// </summary>
    public interface IEntityConfigurationTypeFinder : ITypeFinder
    { }
}