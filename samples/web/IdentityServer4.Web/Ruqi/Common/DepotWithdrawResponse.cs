﻿namespace FlyingFish.Mobile.Ruqi.Common
{
    /// <summary>
    /// 存管提现响应模型
    /// </summary>
    public class DepotWithdrawResponse
    {
        /// <summary>
        /// 提款H5链接地址(点击该地址可直接打开提款页面)
        /// </summary>
        public string url { get; set; }
    }
}