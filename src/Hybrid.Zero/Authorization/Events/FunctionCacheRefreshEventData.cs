﻿// -----------------------------------------------------------------------
//  <copyright file="FunctionCacheRefreshEventData.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-08-02 17:56</last-date>
// -----------------------------------------------------------------------

using Hybrid.EventBuses;

namespace Hybrid.Zero.Authorization.Events
{
    /// <summary>
    /// 功能信息缓存刷新事件源
    /// </summary>
    public class FunctionCacheRefreshEventData : EventDataBase
    { }
}