# Hybrid

## Project Status

[![NuGet Pre Release](https://img.shields.io/nuget/vpre/Hybrid.svg)](https://www.nuget.org/packages/Hybrid/)
![.NET Core](https://github.com/ArcherTrister/Hybrid/workflows/.NET%20Core/badge.svg)
[![Build Status](https://dev.azure.com/ArcherTrister/Hybrid/_apis/build/status/ArcherTrister.Hybrid?branchName=master)](https://dev.azure.com/ArcherTrister/Hybrid/_build/latest?definitionId=3&branchName=master)

## Administration UI Preview

- Quartz UI

![Quartz-Admin-Preview](docs/Images/Quartz-Admin-Preview1.png)
![Quartz-Admin-Preview](docs/Images/Quartz-Admin-Preview2.png)
![Quartz-Admin-Preview](docs/Images/Quartz-Admin-Preview3.png)
![Quartz-Admin-Preview](docs/Images/Quartz-Admin-Preview4.png)
![Quartz-Admin-Preview](docs/Images/Quartz-Admin-Preview5.png)

- IdentityServer UI

![IdentityServer-Admin-Preview](docs/Images/IdentityServer-Admin-Preview1.png)
![IdentityServer-Admin-Preview](docs/Images/IdentityServer-Admin-Preview4.png)
![IdentityServer-Admin-Preview](docs/Images/IdentityServer-Admin-Preview2.png)
![IdentityServer-Admin-Preview](docs/Images/IdentityServer-Admin-Preview3.png)

## Plan

- 设计一个Logo

# 说明
### ①参考<a href="https://github.com/aspnetboilerplate/aspnetboilerplate" target="_blank">ABP</a>
### ②参考<a href="https://github.com/i66soft/osharp" target="_blank">Osharp</a>
### ③参考<a href="https://github.com/tanmingchao/esoftor-master" target="_blank">Esoftor</a>
### 二零二零年一月十九号正式开源

## 特点
### 引入泛型控制器
### Quartz仪表板
### IdentityServer管理后台
### Identity管理后台(待集成)
### 阶梯算法

> ClaimTypes【Hybrid】

> Hybrid.AssemblyInfo

> Hybrid.AspNetCore.AssemblyInfo




