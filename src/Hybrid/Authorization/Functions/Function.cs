﻿// -----------------------------------------------------------------------
//  <copyright file="Function.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor></last-editor>
//  <last-date>2017-09-14 20:13</last-date>
// -----------------------------------------------------------------------

using System.ComponentModel;

namespace Hybrid.Authorization.Functions
{
    /// <summary>
    /// 实体类：功能信息
    /// </summary>
    [Description("功能信息")]
    public class Function : FunctionBase
    { }
}