﻿// -----------------------------------------------------------------------
//  <copyright file="IDoService" company="cn.lxking">
//      Copyright (c) 2014 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2020-01-13 20:35:19</last-date>
// -----------------------------------------------------------------------

namespace Test.Web.Services
{
    public interface IDoService<T>
    {
        void SayHello();
    }
}