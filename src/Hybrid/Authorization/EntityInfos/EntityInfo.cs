﻿// -----------------------------------------------------------------------
//  <copyright file="EntityInfo.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor></last-editor>
//  <last-date>2017-09-14 15:31</last-date>
// -----------------------------------------------------------------------

using System.ComponentModel;

namespace Hybrid.Authorization.EntityInfos
{
    /// <summary>
    /// 实体类：实体信息类
    /// </summary>
    [Description("实体信息类")]
    public class EntityInfo : EntityInfoBase
    { }
}