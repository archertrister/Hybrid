﻿namespace FlyingFish.Mobile.Ruqi.Dtos
{
    /// <summary>
    /// 银行信息
    /// </summary>
    public class RQBankInfo
    {
        /// <summary>
        /// 银行名称
        /// </summary>
        public string bankName { get; set; }

        /// <summary>
        /// 银行代码
        /// </summary>
        public string bankCode { get; set; }
    }
}