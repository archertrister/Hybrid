﻿// -----------------------------------------------------------------------
//  <copyright file="RoleLimitAttribute.cs" company="Hybrid开源团队">
using System;

namespace Hybrid.Authorization
{
    /// <summary>
    /// 指定功能只允许特定角色可以访问
    /// </summary>
    public class RoleLimitAttribute : Attribute
    { }
}