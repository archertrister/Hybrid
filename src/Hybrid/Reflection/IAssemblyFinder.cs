﻿// -----------------------------------------------------------------------
//  <copyright file="IAssemblyFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2017-08-15 23:25</last-date>
// -----------------------------------------------------------------------

using Hybrid.Dependency;
using Hybrid.Finders;

using System.Reflection;

namespace Hybrid.Reflection
{
    /// <summary>
    /// 定义程序集查找器
    /// </summary>
    [IgnoreDependency]
    public interface IAssemblyFinder : IFinder<Assembly>
    { }
}