﻿// -----------------------------------------------------------------------
//  <copyright file="EntityRoleInputDto.cs" company="ESoftor开源团队">
//      Copyright (c) 2014-2018 ESoftor. All rights reserved.
//  </copyright>
//  <site>http://www.lxking.cn</site>
//  <last-editor>郭明锋</last-editor>
//  <last-date>2018-07-03 23:11</last-date>
// -----------------------------------------------------------------------

using ESoftor.Permission.Security;

using System;

namespace ESoftor.Web.Security.Dtos
{
    /// <summary>
    /// 输入DTO：实体角色信息
    /// </summary>
    public class EntityRoleInputDto : EntityRoleInputDtoBase<Guid>
    { }
}