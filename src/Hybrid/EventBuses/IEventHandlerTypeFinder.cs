﻿// -----------------------------------------------------------------------
//  <copyright file="IEventHandlerTypeFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-07-28 23:14</last-date>
// -----------------------------------------------------------------------

using Hybrid.Reflection;

namespace Hybrid.EventBuses
{
    /// <summary>
    /// 定义事件处理器类型查找器
    /// </summary>
    public interface IEventHandlerTypeFinder : ITypeFinder
    { }
}