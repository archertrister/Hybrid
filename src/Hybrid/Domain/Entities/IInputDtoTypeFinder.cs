﻿// -----------------------------------------------------------------------
//  <copyright file="IInputDtoTypeFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor></last-editor>
//  <last-date>2017-09-17 11:44</last-date>
// -----------------------------------------------------------------------

using Hybrid.Reflection;

namespace Hybrid.Domain.Entities
{
    /// <summary>
    /// 定义<see cref="IInputDto{T}"/>类型查找器
    /// </summary>
    public interface IInputDtoTypeFinder : ITypeFinder
    { }
}