﻿// -----------------------------------------------------------------------
//  <copyright file="AuditPropertyOutputDto.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-08-02 18:43</last-date>
// -----------------------------------------------------------------------

using Hybrid.Domain.Entities;

namespace Hybrid.Application.Services.Audits.Dtos
{
    /// <summary>
    /// 输出DTO：实体属性审计
    /// </summary>
    public class AuditPropertyOutputDto : IOutputDto
    {
        /// <summary>
        /// 获取或设置 名称
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// 获取或设置 字段
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 获取或设置 旧值
        /// </summary>
        public string OriginalValue { get; set; }

        /// <summary>
        /// 获取或设置 新值
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// 获取或设置 数据类型
        /// </summary>
        public string DataType { get; set; }
    }
}