﻿// -----------------------------------------------------------------------
//  <copyright file="ITypeFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2017-08-15 23:23</last-date>
// -----------------------------------------------------------------------

using Hybrid.Dependency;
using Hybrid.Finders;

using System;

namespace Hybrid.Reflection
{
    /// <summary>
    /// 定义类型查找行为
    /// </summary>
    [IgnoreDependency]
    public interface ITypeFinder : IFinder<Type>
    { }
}