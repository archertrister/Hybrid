﻿// -----------------------------------------------------------------------
//  <copyright file="EntityRoleInputDto.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-07-03 23:11</last-date>
// -----------------------------------------------------------------------

using Hybrid.Zero.Authorization.Dtos;

using System;

namespace Hybrid.Web.Authorization.Dtos
{
    /// <summary>
    /// 输入DTO：实体角色信息
    /// </summary>
    public class EntityRoleInputDto : EntityRoleInputDtoBase<Guid>
    { }
}