﻿// -----------------------------------------------------------------------
//  <copyright file="GenerateCodeInputDto.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2019-01-06 22:37</last-date>
// -----------------------------------------------------------------------

namespace Hybrid.Zero.Systems.Dtos
{
    /// <summary>
    /// 代码生成输入DTO
    /// </summary>
    public class GenerateCodeInputDto
    { }
}