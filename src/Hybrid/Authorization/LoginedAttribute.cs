﻿using System;

namespace Hybrid.Authorization
{
    /// <summary>
    /// 指定功能需要登录才能访问
    /// </summary>
    public class LoggedInAttribute : Attribute
    { }
}