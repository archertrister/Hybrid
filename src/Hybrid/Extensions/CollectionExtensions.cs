﻿// -----------------------------------------------------------------------
//  <copyright file="CollectionExtensions.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-12-30 22:24</last-date>
// -----------------------------------------------------------------------

using Hybrid.Data;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Hybrid.Extensions
{
    /// <summary>
    /// 集合扩展方法
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// 如果条件成立，添加项
        /// </summary>
        public static void AddIf<T>(this ICollection<T> collection, T value, bool flag)
        {
            Check.NotNull(collection, nameof(collection));
            if (flag)
            {
                collection.Add(value);
            }
        }

        /// <summary>
        /// 如果条件成立，添加项
        /// </summary>
        public static void AddIf<T>(this ICollection<T> collection, T value, Func<bool> func)
        {
            Check.NotNull(collection, nameof(collection));
            if (func())
            {
                collection.Add(value);
            }
        }

        /// <summary>
        /// 如果不存在，添加项
        /// </summary>
        public static void AddIfNotExist<T>(this ICollection<T> collection, T value, Func<T, bool> existFunc = null)
        {
            Check.NotNull(collection, nameof(collection));
            bool exists = existFunc == null ? collection.Contains(value) : existFunc(value);
            if (!exists)
            {
                collection.Add(value);
            }
        }

        /// <summary>
        /// 如果不为空，添加项
        /// </summary>
        public static void AddIfNotNull<T>(this ICollection<T> collection, T value) where T : class
        {
            Check.NotNull(collection, nameof(collection));
            if (value != null)
            {
                collection.Add(value);
            }
        }

        /// <summary>
        /// 获取对象，不存在对使用委托添加对象
        /// </summary>
        public static T GetOrAdd<T>(this ICollection<T> collection, Func<T, bool> selector, Func<T> factory)
        {
            Check.NotNull(collection, nameof(collection));
            T item = collection.FirstOrDefault(selector);
            if (item == null)
            {
                item = factory();
                collection.Add(item);
            }

            return item;
        }

        /// <summary>
        /// 判断集合是否为null或空集合
        /// </summary>
        public static bool IsNullOrEmpty<T>(this ICollection<T> collection)
        {
            return collection == null || collection.Count == 0;
        }

        /// <summary>
        /// Adds an item to the collection if it's not already in the collection.
        /// </summary>
        /// <param name="source">Collection</param>
        /// <param name="item">Item to check and add</param>
        /// <typeparam name="T">Type of the items in the collection</typeparam>
        /// <returns>Returns True if added, returns False if not.</returns>
        public static bool AddIfNotContains<T>(this ICollection<T> source, T item)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source");
            }

            if (source.Contains(item))
            {
                return false;
            }

            source.Add(item);
            return true;
        }

        public static int FindIndexOrDefault<T>(this List<T> ts, Predicate<T> match) where T : class
        {
            try
            {
                return ts.FindIndex(match);
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }

    ///// <summary>
    ///// 集合扩展方法
    ///// </summary>
    //public static class CollectionExtensions
    //{
    //    /// <summary>
    //    /// Checks whatever given collection object is null or has no item.
    //    /// </summary>
    //    public static bool IsNullOrEmpty<T>(this ICollection<T> source)
    //    {
    //        return source == null || source.Count <= 0;
    //    }

    //    /// <summary>
    //    /// 如果条件成立，添加项
    //    /// </summary>
    //    public static void AddIf<T>(this ICollection<T> collection, T value, bool flag)
    //    {
    //        Check.NotNull(collection, nameof(collection));
    //        if (flag)
    //        {
    //            collection.Add(value);
    //        }
    //    }

    //    /// <summary>
    //    /// 如果条件成立，添加项
    //    /// </summary>
    //    public static void AddIf<T>(this ICollection<T> collection, T value, Func<bool> func)
    //    {
    //        Check.NotNull(collection, nameof(collection));
    //        if (func())
    //        {
    //            collection.Add(value);
    //        }
    //    }

    //    /// <summary>
    //    /// 如果不存在，添加项
    //    /// </summary>
    //    public static void AddIfNotExist<T>(this ICollection<T> collection, T value, Func<T, bool> existFunc = null)
    //    {
    //        Check.NotNull(collection, nameof(collection));
    //        bool exists = existFunc == null ? collection.Contains(value) : existFunc(value);
    //        if (!exists)
    //        {
    //            collection.Add(value);
    //        }
    //    }

    //    /// <summary>
    //    /// 如果不为空，添加项
    //    /// </summary>
    //    public static void AddIfNotNull<T>(this ICollection<T> collection, T value) where T : class
    //    {
    //        Check.NotNull(collection, nameof(collection));
    //        if (value != null)
    //        {
    //            collection.Add(value);
    //        }
    //    }

    //    /// <summary>
    //    /// 获取对象，不存在对使用委托添加对象
    //    /// </summary>
    //    public static T GetOrAdd<T>(this ICollection<T> collection, Func<T, bool> selector, Func<T> factory)
    //    {
    //        Check.NotNull(collection, nameof(collection));
    //        T item = collection.FirstOrDefault(selector);
    //        if (item == null)
    //        {
    //            item = factory();
    //            collection.Add(item);
    //        }

    //        return item;
    //    }

    //}
}