﻿namespace FlyingFish.Mobile.Ruqi.Dtos
{
    /// <summary>
    /// 合同协议
    /// </summary>
    public class RQContract
    {
        /// <summary>
        /// 合同名称
        /// </summary>
        public string contractName { get; set; }

        /// <summary>
        /// 合同url
        /// </summary>
        public string url { get; set; }
    }
}