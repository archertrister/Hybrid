﻿namespace Hybrid.Data
{
    /// <summary>
    /// 性别类型
    /// </summary>
    public enum GenderType
    {
        /// <summary>
        /// 男
        /// </summary>
        Male,

        /// <summary>
        /// 女
        /// </summary>
        Female,

        /// <summary>
        /// 保密
        /// </summary>
        Security,

        /// <summary>
        /// 未知
        /// </summary>
        Uncharted
    }
}