﻿// -----------------------------------------------------------------------
//  <copyright file="IHybridModuleTypeFinder.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2018-12-30 23:06</last-date>
// -----------------------------------------------------------------------

using Hybrid.Reflection;

namespace Hybrid.Core.Modules
{
    /// <summary>
    /// Hybrid模块类型查找器
    /// </summary>
    public interface IHybridModuleTypeFinder : ITypeFinder
    { }
}