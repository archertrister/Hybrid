﻿namespace FlyingFish.Mobile.Ruqi.Common
{
    /// <summary>
    /// 跳转到机构还款响应模型
    /// </summary>
    public class RepaymentExtResponse
    {
        /// <summary>
        /// 还款H5链接地址(点击该地址可直接打开还款页面)
        /// </summary>
        public string url { get; set; }
    }
}