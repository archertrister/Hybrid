﻿// -----------------------------------------------------------------------
//  <copyright file="HealthChecksModule.cs" company="cn.lxking">
//      Copyright © 2019-2020 Hybrid. All rights reserved.
//  </copyright>
//  <site>https://www.lxking.cn</site>
//  <last-editor>ArcherTrister</last-editor>
//  <last-date>2019-10-05 19:39</last-date>
// -----------------------------------------------------------------------

using System.ComponentModel;

namespace Hybrid.AspNetCore.Diagnostics
{
    /// <summary>
    /// 程序健康检查模块
    /// </summary>
    [Description("HealthChecks模块")]
    public class HealthChecksModule : HealthChecksModuleBase
    { }
}